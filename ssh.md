[< Dokumentace](README.md)
## Jak na SSH

### SS-CO?
SSH (Secure Shell) je v informatice označení pro program a zároveň pro zabezpečený komunikační protokol v počítačových sítích. Zkrátka a jednoduše je to program, který vám umožňuje se bezpečně připojit na server a komunikovat s ním.

### PuTTY
PuTTY je SSH klient, který je již předinstalován v učebnách, tudíž není potřeba nic dalšího instalovat. Pokud si však chcete PuTTY stáhnout doma, nebo jinde můžete tak učinit [zde](http://www.putty.org/).

Další kroky už jsou naprosto identické jako v KiTTY.

###  KiTTY
Kitty je fork PuTTY, ketrý nabízí bugfixy a další vylepšení. KiTTY je naprosto zdarma a není složité se s tímto programem naučit. Můžete si to stáhnout [zde](http://www.9bis.net/kitty/).

#### Přihlášení na Avavu
Po spuštění putty.exe/kitty.exe by se vám mělo zobrazit okno. Vlevo se nachází nastavení, které nebudu zde popisovat, dole seznam uložených serverů a nakonec nahoře vstupní pole pro server. Jednoduše zadejte adresu Avavy (svs.gyarab.cz) do pole označeného "Host Name (or IP address)" a zmáčkněte vlevo dole open. 
Následně by se měla zobrazit konzole, která se vás zeptá na jméno a následně na heslo, které vám bylo zobrazeno při registraci. Po přihlášení jste již úspěšně připojeni na Avavu.

## První kroky
Při prvním přihlášení se vám zobrazí uvítací program s informacemi o Avavě. Po ukončení tohoto programu budete přivítání [fish shellem](https://fishshell.com/). Fish shell je jednoduché textové uživatelské prostředí.

### Změna hesla
Pokud nejste spokojeni s náhodně generovaným heslem, je možné provést změnu hesla. Pro změnu hesla musíte být přihlášení přes SSH na Avavě a zadat příkaz "passwd". Samozřejmě bez uvozovek. Následně se řiďte pokyny v terminálu.