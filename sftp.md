[< Dokumentace](README.md)
## Přenos souborů
Přenos souborů zajišťuje SFTP, což je v podstatě FTP přes SSH.

### WinSCP
Tento program je již předinstalován na počítačích v učebnách, tudíž není potřeba nic dalšího instalovat. Pokud si však tento program chcete stáhnout doma, nebo jinde můžete tak učinit [zde](https://winscp.net).

#### Přihlášení - WinSCP
Po spuštění klienta se vám zobrazí seznam uložených serverů. Avavu můžete buď přidat do vašeho seznamu, nebo pokaždé vyplnit informace znovu. Z dropdownu "File Protocol" vyberte SFTP, do hostname zadejte adresu Avavy (svs.gyarab.cz), port zadejte 22 a do políček username a password zadejte vaše jméno a heslo, kterým se přihlašujete na Avavě.

Pokud chcete informace uložit, klikněte na tlačítko Save. Vyskočí vá okno kde si můžete server pojmenovat, popřípadě si vytvořit zástupce pro připojení na ploše.

Následně již stačí kliknout na tlačítko Login. Je možné, že vám vyskočí okno, zda chcete důvěřovat enkrypčnímu klíči Avavy. Klikněte na tlačítko Ano.

Zobrazí se vám 2 panely se seznamy souborů. V prvním (levém) je seznam souborů na vašem PC. V druhém (pravém) panelu se vám zobrazí seznam souborů na serveru. Následně již můžete přetahovat soubory dle libosti mezi vaším PC a Avavou.

### Filezilla
Další z často používaných FTP klientů je FileZilla. FileZilla je naprosto zdarma a není složité se s tímto programem naučit. Můžete si ho stáhnout [zde](https://filezilla-project.org/).

#### Přihlášení - FileZilla
Po nainstalování a spuštění klienta (server neinstalujte) se vám zobrazí okno s výpisem souborů. Nejlehčí způsob jak se připojit je využít lištu rychlého připojení v horní části obrazovky. Skládá se ze 4 polí: Hostitel, Uživatelské jméno, Heslo a Port.

Do kolonky hostitel napište adresu Avavy (svs.gyarab.cz), do Uživatelského jména vaše uživatelské jméno, do hesla vaše heslo (toto heslo není nikde ukládáno) a do portu číslo 22. Následně již stačí pouze kliknout na tlačítko Rychlé připojení. V druhém (pravém) panelu se vám zobrazí seznam souborů na serveru. Následně již můžete přetahovat soubory dle libosti mezi vaším PC a Avavou.
